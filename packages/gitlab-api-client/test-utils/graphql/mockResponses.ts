import type {
  CreateProjectBranchResult,
  getProjectUserPermissionsResult,
  SearchProjectBranchesResult,
} from '../../src/graphql';

export const createProjectBranch: CreateProjectBranchResult = {
  createBranch: {
    branch: {
      name: 'new-branch-test',
    },
    errors: [],
  },
};

export const getProjectUserPermissions: getProjectUserPermissionsResult = {
  project: {
    userPermissions: {
      createMergeRequestIn: true,
      pushCode: false,
      readMergeRequest: true,
    },
  },
};

export const searchProjectBranches: SearchProjectBranchesResult = {
  project: {
    repository: {
      branchNames: ['foo', 'bar'],
    },
  },
};
