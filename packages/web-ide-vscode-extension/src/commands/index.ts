import * as vscode from 'vscode';
import {
  CHECKOUT_BRANCH_COMMAND_ID,
  GO_TO_GITLAB_COMMAND_ID,
  GO_TO_PROJECT_COMMAND_ID,
  SHARE_YOUR_FEEDBACK_COMMAND_ID,
  START_REMOTE_COMMAND_ID,
  OPEN_REMOTE_WINDOW_COMMAND_ID,
  RELOAD_WITH_WARNING_COMMAND_ID,
} from '../constants';
import checkoutBranch from './checkoutBranch';
import goToGitLab from './goToGitLab';
import goToProject from './goToProject';
import shareYourFeedback from './shareYourFeedback';
import startRemote from './startRemote';
import { CommandsInitialData } from '../types';
import openRemoteWindow from './openRemoteWindow';
import reloadWithWarning from './reloadWithWarning';

export const registerCommands = (
  disposables: vscode.Disposable[],
  dataPromise: Thenable<CommandsInitialData>,
) => {
  disposables.push(
    vscode.commands.registerCommand(CHECKOUT_BRANCH_COMMAND_ID, checkoutBranch(dataPromise)),
    vscode.commands.registerCommand(GO_TO_GITLAB_COMMAND_ID, goToGitLab(dataPromise)),
    vscode.commands.registerCommand(GO_TO_PROJECT_COMMAND_ID, goToProject(dataPromise)),
    vscode.commands.registerCommand(OPEN_REMOTE_WINDOW_COMMAND_ID, openRemoteWindow(dataPromise)),
    vscode.commands.registerCommand(SHARE_YOUR_FEEDBACK_COMMAND_ID, shareYourFeedback),
    vscode.commands.registerCommand(START_REMOTE_COMMAND_ID, startRemote),
    vscode.commands.registerCommand(RELOAD_WITH_WARNING_COMMAND_ID, reloadWithWarning),
  );
};
