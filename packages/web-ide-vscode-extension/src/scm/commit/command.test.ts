import * as vscode from 'vscode';
import { COMMAND_COMMIT } from '@gitlab/vscode-mediator-commands';
import { FileStatusType, IFileStatus } from '@gitlab/web-ide-fs';
import { IReadonlySourceControlViewModel } from '../types';
import { generateCommitMessage } from './generateCommitMessage';
import { promptBranchName } from './promptBranchName';
import { factory } from './command';
import { getCommitPayload } from './getCommitPayload';
import { RELOAD_COMMAND_ID } from '../../constants';
import { TEST_PROJECT } from '../../../test-utils';
import { showSuccessMessage } from './showSuccessMessage';

jest.mock('./promptBranchName');
jest.mock('./generateCommitMessage');
jest.mock('./showSuccessMessage');

const TEST_COMMIT_MESSAGE = 'Hello world! Test commit!';
const TEST_GENERATED_COMMIT_MESSAGE = 'Genearted commit message:\n\n123';
const TEST_BRANCH_NAME = 'foo-branch';
const TEST_BRANCH_MR_URL = 'https://gitlab.example.com/mr/1';
const TEST_BRANCH_SELECTION = {
  branchName: 'foo-branch-patch-123',
  isNewBranch: false,
};
const TEST_COMMIT_ID = '000000111111';
const TEST_STATUS: IFileStatus[] = [{ type: FileStatusType.Deleted, path: '/README.md' }];

describe('scm/commit/command', () => {
  let viewModel: jest.MockedObject<IReadonlySourceControlViewModel>;
  let command: () => Promise<void>;

  beforeEach(() => {
    viewModel = {
      getCommitMessage: jest.fn().mockReturnValue(TEST_COMMIT_MESSAGE),
      getStatus: jest.fn().mockReturnValue(TEST_STATUS),
    };
    jest.mocked(promptBranchName).mockResolvedValue(TEST_BRANCH_SELECTION);
    jest.mocked(generateCommitMessage).mockReturnValue(TEST_GENERATED_COMMIT_MESSAGE);
  });

  describe('with default dependencies', () => {
    beforeEach(() => {
      command = factory({
        project: TEST_PROJECT,
        branchName: TEST_BRANCH_NAME,
        branchMergeRequestUrl: TEST_BRANCH_MR_URL,
        commitId: TEST_COMMIT_ID,
        viewModel,
      });
    });

    describe('default', () => {
      beforeEach(async () => {
        await command();
      });

      it('calls commit mediator command', () => {
        expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
          COMMAND_COMMIT,
          getCommitPayload({
            status: TEST_STATUS,
            commitMessage: TEST_COMMIT_MESSAGE,
            startingSha: TEST_COMMIT_ID,
            ...TEST_BRANCH_SELECTION,
          }),
        );
      });

      it('shows success message', () => {
        expect(showSuccessMessage).toHaveBeenCalledWith({
          project: TEST_PROJECT,
          branchName: TEST_BRANCH_SELECTION.branchName,
          mrUrl: TEST_BRANCH_MR_URL,
        });
      });

      it('calls reload command', () => {
        expect(vscode.commands.executeCommand).toHaveBeenCalledWith(RELOAD_COMMAND_ID, {
          ref: TEST_BRANCH_SELECTION.branchName,
        });
      });
    });

    describe('when commit fails', () => {
      const testError = new Error();

      beforeEach(async () => {
        jest.spyOn(console, 'error').mockImplementation();
        jest.mocked(vscode.commands.executeCommand).mockRejectedValue(testError);
        await command();
      });

      it('logs and shows error message', () => {
        // eslint-disable-next-line no-console
        expect(console.error).toHaveBeenCalledWith(testError);

        expect(vscode.window.showErrorMessage).toHaveBeenCalledWith(
          expect.stringContaining('Error:'),
        );
      });

      it('does not call reload command', () => {
        expect(vscode.commands.executeCommand).toHaveBeenCalledTimes(1);
        expect(vscode.commands.executeCommand).not.toHaveBeenCalledWith(
          RELOAD_COMMAND_ID,
          expect.anything(),
        );
      });

      it('does not show success message', () => {
        expect(vscode.window.showInformationMessage).not.toHaveBeenCalled();
      });
    });

    describe('with no branch selection', () => {
      beforeEach(async () => {
        jest.mocked(promptBranchName).mockResolvedValue(undefined);
        await command();
      });

      it('does not commit', () => {
        expect(vscode.commands.executeCommand).not.toHaveBeenCalled();
      });

      it('does not show any message', () => {
        expect(vscode.window.showInformationMessage).not.toHaveBeenCalled();
      });
    });

    describe('with newBranch selection', () => {
      beforeEach(async () => {
        jest.mocked(promptBranchName).mockResolvedValue({
          ...TEST_BRANCH_SELECTION,
          isNewBranch: true,
        });
        await command();
      });

      it('does not include branch MR URL in showSuccessMessage', () => {
        expect(showSuccessMessage).toHaveBeenCalledWith({
          project: TEST_PROJECT,
          branchName: TEST_BRANCH_SELECTION.branchName,
          mrUrl: '',
        });
      });
    });

    describe('when viewModel.getCommitMessage is empty', () => {
      beforeEach(async () => {
        viewModel.getCommitMessage.mockReturnValue('');
        await command();
      });

      it('generates commit message for payload', () => {
        expect(generateCommitMessage).toHaveBeenCalledWith(TEST_STATUS);
        expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
          COMMAND_COMMIT,
          expect.objectContaining({
            commit_message: TEST_GENERATED_COMMIT_MESSAGE,
          }),
        );
      });
    });

    describe('with empty status', () => {
      beforeEach(async () => {
        viewModel.getStatus.mockReturnValue([]);
        await command();
      });

      it('shows information message', () => {
        expect(vscode.window.showInformationMessage).toHaveBeenCalledWith(
          expect.stringMatching('No changes found'),
        );
      });

      it('does not execute any other commands', () => {
        expect(vscode.commands.executeCommand).not.toHaveBeenCalled();
      });
    });
  });

  describe('with empty project', () => {
    beforeEach(async () => {
      command = factory({
        project: { ...TEST_PROJECT, empty_repo: true },
        branchName: TEST_BRANCH_NAME,
        branchMergeRequestUrl: '',
        commitId: TEST_COMMIT_ID,
        viewModel,
      });
      await command();
    });

    it('does not prompt for branch name', () => {
      expect(promptBranchName).not.toHaveBeenCalled();
    });

    it('triggers commit command', () => {
      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
        COMMAND_COMMIT,
        expect.objectContaining({
          start_sha: undefined,
          branch: TEST_BRANCH_NAME,
        }),
      );
    });
  });
});
