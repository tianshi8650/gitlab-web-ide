export type DeletedFilesLineType = 'file' | 'dir';

export interface IDeletedFilesLogContents {
  readonly files: ReadonlySet<string>;
  readonly directories: ReadonlySet<string>;
}

export interface IDeletedFilesLogReadonly {
  readonly path: string;
  isDeleted(p: string): Promise<boolean>;
  getContents(): Promise<IDeletedFilesLogContents | null>;
  getModifiedTime(): Promise<number>;
}

export interface IDeletedFilesLog extends IDeletedFilesLogReadonly {
  append(type: DeletedFilesLineType, pathArg: string): Promise<void>;
}
