import { createApp } from 'vue';
import App from './components/App.vue';

const root = document.getElementById('main');

if (!root) {
  throw new Error('#main element not found!');
}

createApp(App).mount(root);
